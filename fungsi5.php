<?php
//PASSING BY REFERENCE

function tambah_string(&$str){
    $str = $str . ", Teknik";
    return $str;
}

$string = "Teknik Informatika ";
echo "\$string = $string<br>";
echo tambah_string($string). "<br>";
echo "\$string = $string<br>";

?>
