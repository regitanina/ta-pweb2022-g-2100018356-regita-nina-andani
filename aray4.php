<?php
$arrNilai = array("Vicha"=>90,"Regita"=>85,"Dea"=>75,"Reza"=>80);
echo "Menampilkan isi Array asosiatif dengan foreach : <br>";
foreach ($arrNilai as $nama => $nilai) {
    echo "Nilai $nama = $nilai<br>";
}

reset($arrNilai); 
echo "<br>Menampilkan isi array asosiatif dengan WHILE dan LIST : <br>";
while (list($nama, $nilai) = each($arrNilai)) {
    echo "Nilai $nama = $nilai <br>";
}
?>